import React from 'react';
import Login  from './pages/Login/login';
import SignUp from './pages/SignUp/signup';
import MyExercises from './pages/MyExercises/myexercises';
import MyTeachers from './pages/MyTeachers/myteachers';
import NewExercise from './pages/NewExercise/newexercise';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import icon from '../assets/list.png';

const myExercises = createStackNavigator({
    "Meus Treinos": MyExercises,
},
    {
        defaultNavigationOptions: ({navigation}) => ({
                title: 'Meus Treinos',
                headerStyle: {
                    backgroundColor: "#72bcd4",
                    height: 60,
                    width: '100%'
                },
                headerMode: 'float',
                headerTitle: 'Meus Treinos',
                headerTintColor: '#000',
                headerTitleStyle: {
                    fontSize: 24,
                    fontWeight: "bold",
                    textAlign: "center",
                    flex: 1,                      
                },
                headerLeft: (
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                        source = {icon}
                        style = {{tintColor: 'white', marginLeft: 10}}
                        />

                    </TouchableOpacity>
                ),
                headerRight: (
                    <View/>
                )
                }),
    },
);
const myTeachers = createStackNavigator({
    "Meus Alunos": MyTeachers,
},
    {
        defaultNavigationOptions: ({navigation}) => ({
                title: 'Meus Alunos',
                headerStyle: {
                    backgroundColor: "#72bcd4",
                    height: 60,
                    width: '100%'
                },
                headerMode: 'float',
                headerTitle: 'Meus Alunos',
                headerTintColor: '#000',
                headerTitleStyle: {
                    fontSize: 24,
                    fontWeight: "bold",
                    textAlign: "center",
                    flex: 1,                      
                },
                headerLeft: (
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                        source = {icon}
                        style = {{tintColor: 'white', marginLeft: 10}}
                        />

                    </TouchableOpacity>
                ),
                headerRight: (
                    <View/>
                )
                }),
    },
);
const newExercise = createStackNavigator({
    "Novo Treino": NewExercise
},
    {
        defaultNavigationOptions: ({navigation}) => {
            return {
                headerStyle: {
                    backgroundColor: "#72bcd4"
                },
                headerTitle: 'Novo Treino: João',
                headerTintColor: '#000',
                headerTitleStyle: {
                    fontSize: 24,
                    fontWeight: "bold",
                    textAlign: "center",
                    flex: 1,                      
                },
                headerLeft: (
                    <TouchableOpacity onPress={() => navigation.openDrawer()}>
                        <Image
                        source = {icon}
                        style = {{tintColor: 'white', marginLeft: 10}}
                        />

                    </TouchableOpacity>
                ),
                headerRight: (
                    <View/>
                )
            }
        }
    },
);
const DrawerNavigationOptions = createDrawerNavigator({
    
    "Meus Treinos": myExercises,
    "Novo Treino": newExercise,
    "Meus Alunos": myTeachers,
    "Sair da Conta": {
        screen: Login
    },
    "Fechar": {
        screen: SignUp
    },

},
{
    hideStatusBar: true,
    drawerBackgroundColor: '#fff',
    overlayColor: '#e6e6e6',
    contentOptions: {
      activeTintColor: '#fff',
      activeBackgroundColor: '#000'
    },
    initialRouteName: 'Meus Alunos'
});

export default createAppContainer(DrawerNavigationOptions);