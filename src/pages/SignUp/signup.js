import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import firebase from 'react-native-firebase';

const db = firebase.firestore();
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.navigationOptions = {
            title: 'SignUp'
        }
        this.state = {
            email: '',
            password: '',
            name: ''
        }
    }

    register = () => {

        try {
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then(res => {
                return db.collection('users').doc(res.user._user.uid).set({
                    name: this.state.name,
                    email: this.state.email
                });
            }).then(() => {
                console.log('this -> ', this);
                this.props.navigation.navigate("MyExercises");
            });
        } catch (error) {
            console.log("error", error);
        }

    }
    render() {
        return (
            <View style={style.alignItems}>
                <Text style={style.texts}>Nova conta</Text>
                
                <TextInput onChangeText={(name) => this.setState({name})} style={style.inputs} value={this.state.name}placeholder="Digite seu nome.."/>
                <TextInput onChangeText={(email) => this.setState({email})} style={style.inputs} value={this.state.email} placeholder="Digite seu e-mail..."/>
                <TextInput onChangeText={(password) => this.setState({password})} style={style.inputs} value={this.state.password} secureTextEntry={true} placeholder="Digite sua senha..."/>
                <TouchableOpacity onPress={this.register} style={style.buttons}>
                    <Text style={style.textColor}>Realizar cadastro</Text>
                </TouchableOpacity>
            </View>
        )
    }
};

const style = StyleSheet.create({

    alignItems: {
        display: "flex",
        backgroundColor: '#72bcd4',
        justifyContent: "center",
        alignItems: "center",
        height: '100%'
    },
    inputs: {
        padding: 10,
        backgroundColor: '#e6e6e6',
        marginBottom: 15,
        borderRadius: 10,
        width: '70%',
        borderStyle: "solid",
        borderColor: "#000",
        borderWidth: 2
    },
    buttons: {
        backgroundColor: '#fff',
        paddingVertical: 15,
        paddingHorizontal: 10,
        width: '50%',
        borderRadius: 10,
        borderStyle: "solid",
        borderColor: "#000",
        borderWidth: 2
    },
    texts: {
        marginTop: 25,
        marginBottom: 25,
        fontWeight: "bold",
        fontSize: 24,
        justifyContent: "center"
    },

    textColor: {
        color: '#000',
        justifyContent: "center",
        fontWeight: "bold",
        textAlign: "center"
    }
});