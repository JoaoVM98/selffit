import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import firebase from 'react-native-firebase';

const db = firebase.firestore();
const user = firebase.auth().currentUser;
export default class MyTeachers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            students: []
        }
    }

    componentDidMount() {
        db.collection("professors").doc("ogx3d2zDnGA5exXy90PI").get().then(docs => {
            console.log('docs -- ', docs.data().students);
            this.setState({students: docs.data().students});
        })
    }

    renderListItem = ({item}) => {
        return (
            <View style={styles.listStyle}>
                <Text style={styles.listText}>Nome: {item.name}</Text>
                <Text style={styles.listText}>E-mail: {item.email}</Text>
                <View style = {{width: '100%', marginVertical: 8, alignItems: "center", justifyContent: "center", display:"flex"}}>
                    <TouchableOpacity style={styles.exerciseButton}>
                        <Text style={styles.actionText}>Elaborar Treino</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (            
            <View style={styles.container}>
                <FlatList style={{backgroundColor: "#e4e4e4"}}
                data={this.state.students}
                renderItem={this.renderListItem}
                keyExtractor={item => item.id}
                />
            </View>
        )
    }

}


const styles = StyleSheet.create({
    actionText: {
        fontWeight: "500",
        fontSize: 16,
        textAlign: "center",
        color: "#fff"
    },
    exerciseButton: {
        borderStyle: 'solid',
        borderRadius: 5,
        borderWidth: 1,
        padding: 10,
        width: 200,
        backgroundColor: "#72bcd4",
        borderColor: "#000"
    },
    removeButton: {
        borderColor: "#000",
        backgroundColor: "#580000"
    },
    practiceButton: {
        borderColor: "#000",
        backgroundColor: "#72bcd4"
    },
    actionButtons: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: "100%",
        paddingVertical: 5
    },
    container: {
        backgroundColor: '#fff',
        height: '100%',
        width: '100%'
    },
    button: {
        borderRadius: 400,
        borderColor: "#000",
        borderWidth: 2,
        width: 40,
        height: 40,
        backgroundColor: "#72bcd4",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 65,
        right: 35
    },
    buttonText: {
        fontSize: 20,
        fontWeight: "bold"
    },
    listStyle: {
        width: '100%',
        height: "auto",
        backgroundColor: "#fff",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-start",
        borderBottomColor: "#000",
        borderBottomWidth: 2,
        borderStyle: "solid"
    },
    listText: {
        paddingLeft: 10,
        fontWeight: "bold",
        fontSize: 16
    }
})