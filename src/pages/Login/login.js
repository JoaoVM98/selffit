import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import firebase from 'react-native-firebase';
import React, {Component} from 'react';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.navigationOptions = {
            title: 'Login'
        }
        this.state = {
            email: '',
            password: ''
        }
    }

    onChangeEmail = (text) => {
        this.setState({
            email: text
        });
    }
    onChangePassword = (pwd) => {
        this.setState({
            password: pwd
        })
    }
    goToSignUpScreen = () => {
        this.props.navigation.navigate("SignUp");
    }

    login = () => {
        console.log('HERE', this)
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(user => {
            console.log('user logged in -> ', user);
            this.props.navigation.navigate("MyExercises");
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={style.alignItems}>
                <Text style={style.texts}>Seja bem-vindo ao SelfFit!</Text>
                <Text style={style.texts}>Faça aqui seu login</Text>
                <TextInput onChangeText={(email) => this.onChangeEmail(email)} style={style.inputs} value={this.state.email} placeholder="E-mail..."/>
                <TextInput onChangeText={(pwd) => this.onChangePassword(pwd)} style={style.inputs} value={this.state.password} secureTextEntry={true} placeholder="Senha..."/>
                <TouchableOpacity onPress={this.login} style={style.buttons}>
                    <Text style={style.textColor}>Acessar</Text>
                </TouchableOpacity>
                <Text style={style.texts}>Ainda não possui conta?</Text>
                <TouchableOpacity onPress={this.goToSignUpScreen} style={style.buttons}>
                    <Text style={style.textColor}>Cadastre-se aqui!</Text>
                </TouchableOpacity>
            </View>
        )
    }
};

const style = StyleSheet.create({

    alignItems: {
        display: "flex",
        backgroundColor: '#72bcd4',
        justifyContent: "center",
        alignItems: "center",
        height: '100%'
    },
    inputs: {
        padding: 10,
        backgroundColor: '#e6e6e6',
        marginBottom: 15,
        borderRadius: 10,
        width: '70%',
        borderStyle: "solid",
        borderColor: "#000",
        borderWidth: 2
    },
    buttons: {
        backgroundColor: '#fff',
        paddingVertical: 15,
        paddingHorizontal: 10,
        width: '50%',
        borderRadius: 10,
        borderStyle: "solid",
        borderColor: "#000",
        borderWidth: 2
    },
    texts: {
        marginTop: 25,
        marginBottom: 25,
        fontWeight: "bold",
        fontSize: 20,
        justifyContent: "center"
    },

    textColor: {
        color: '#000',
        justifyContent: "center",
        fontWeight: "bold",
        textAlign: "center"
    }
});