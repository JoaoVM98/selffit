import {View, Text, FlatList, TouchableWithoutFeedback, TouchableOpacity, StyleSheet, Modal} from 'react-native';
import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import { TextInput } from 'react-native-gesture-handler';

const db = firebase.firestore();
const choosedExercises = [];
const user = firebase.auth().currentUser;
export default class NewExercise extends Component {
    constructor() {
        super();
        this.state = {
            exercises: [],
            choosedExercises: [],
            showModal: false,
            exerciseTitle: ''
        }
    }

    componentDidMount() {
        var list = [];
        db.collection("exercise_list").get().then(docs => {
            docs.docs.forEach(doc => {
                var exerciseModel = {
                    id: doc.id,
                    isClicked: false,
                    list: []
                }
                doc.data().exercises.forEach(exercise => {
                    exerciseModel.list.push(exercise);
                })
                
                list.push(exerciseModel);
            })
            this.setState({
                exercises: list
            });
        }).catch(err => {
            console.log('error ---- ', err);
        })
    }

    renderListItem = ({item}) => {
        return (
            <View style={styles.listStyle}>
                <TouchableWithoutFeedback>
                    <Text style={styles.listText}>{item.id}</Text>
                </TouchableWithoutFeedback>
                <FlatList data={item.list}
                renderItem={this.renderExerciseList}
                keyExtractor={item => item.name}
                />
            </View>
        )
    }

    renderExerciseList = ({item}) => {
        return (
            <TouchableWithoutFeedback onPress={() => this.addExercise(item)}>
                <View style={styles.subItem}>
                    <Text style={styles.subItemTitle}>{item.name}</Text>
                    <Text style={styles.subItemDescription}>• {item.description}</Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    addExercise = (item) => {
        choosedExercises.push(item);
        console.log('choosedExercises --- ', choosedExercises);
        this.setState({choosedExercises});
    }

    handleSaveExercise = () => {
        if(this.state.exerciseTitle !== '') {
            console.log('this state ', this.state);
            console.log('currentUser -- ', user);
            db.collection("users").doc(user._user.uid).collection("exercises").doc(this.state.exerciseTitle).set({
                exercises: this.state.choosedExercises
            }).then(res => {
                this.props.navigation.navigate("MyExercises");
            });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal animationType={'fade'} transparent={true} visible={this.state.showModal}>
                    <View style={styles.modalContainer}>
                        <Text>Escolha o titulo do seu Treino!</Text>
                        <TextInput style={styles.modalInput} onChangeText={(exerciseTitle) => this.setState({exerciseTitle})}></TextInput>
                        <TouchableOpacity style={[styles.modalButton, styles.modalSaveButton]} onPress={() => this.handleSaveExercise()}>
                            <Text style={styles.modalText}>Salvar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.modalButton, styles.modalCloseButton]} onPress={() => this.setState({showModal:false})}>
                            <Text style={styles.modalText}>Fechar</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <FlatList data={this.state.exercises}
                renderItem={this.renderListItem}
                keyExtractor={item => item.id}
                />
                <TouchableOpacity onPress={() => {this.setState({showModal: true})}} style={styles.button}>
                    <Text>Criar novo treino</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        height: '100%',
        width: '100%'
    },
    headerBar: {
        width: '100%',
        height: 60,
        display: "flex",
        backgroundColor: "#72bcd4",
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 15,
        borderBottomColor: "#000",
        borderBottomWidth: 2,
        borderStyle: "solid"
    },
    headerText: {
        fontSize: 24,
        color: '#000',
        fontWeight: "bold"
    },
    listStyle: {
        width: '100%',
        height: "auto",
        backgroundColor: "#fff",
        paddingVertical: 15,
        paddingLeft: 25,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-start",
        borderBottomColor: "#cacaca",
        borderBottomWidth: 2,
        borderStyle: "solid"
    },
    listText: {
        fontWeight: "bold",
        fontSize: 20,
        textTransform: "uppercase"
    },
    subItem: {
        fontSize: 16,
        marginTop: 5
    },
    subItemTitle: {
        fontStyle: "italic",
        textTransform: "capitalize",
        fontSize: 16
    },
    subItemDescription: {
        marginLeft: 10
    },
    button: {
        borderRadius: 400,
        borderColor: "#000",
        borderWidth: 2,
        width: 150,
        height: 50,
        backgroundColor: "#72bcd4",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 35,
        right: '29%'
    },

    modalContainer: {
        position: "absolute",
        top: '25%',
        left: '8%',
        backgroundColor: "#fff",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: 300,
        width: 300,
        borderColor: "#000",
        borderRadius: 50,
        borderWidth: 2,
        borderStyle: "dotted"
    },

    modalText: {
        fontWeight: "bold",
        fontSize: 20,
        color: "#000"
    },

    modalButton: {
        marginTop: 10,
        paddingHorizontal: 25,
        paddingVertical: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: '60%'
    },

    modalCloseButton: {
        backgroundColor: "#ff0000"
    },

    modalSaveButton: {
        backgroundColor: "#4c924c"
    },

    modalInput: {
        marginTop: 25,
        marginBottom: 15,
        backgroundColor: "#f4f4f4",
        borderBottomWidth: 1,
        borderBottomColor: "#000",
        borderRadius: 15,
        width: '80%'
    }
})