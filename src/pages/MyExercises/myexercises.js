import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import firebase from 'react-native-firebase';
import ExerciseModel from '../../Models/ExerciseModel/exerciseModel';

const db = firebase.firestore();
const user = firebase.auth().currentUser;
export default class MyExercises extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exercises: []
        }
    }

    componentDidMount() {
        var exercises = [];
        db.collection("users").doc(user._user.uid).collection("exercises").get().then(docs => {
            docs.docs.forEach((doc, index) => {
                var exerciseModel = {
                    id: doc.id,
                    list: doc.data().exercises
                }
                
                exercises.push(exerciseModel);
            })
            this.setState({
                exercises
            });
        })
    }

    renderListItem = ({item}) => {
        return (
            <View style={styles.listStyle}>
                <ExerciseModel id={item.id} list = {item.list}/>
                <View style={styles.actionButtons}> 
                    <TouchableOpacity onPress={item.excludeFromDb()} style={[styles.exerciseButton, styles.removeButton]}>
                        <Text style={styles.actionText}>Excluir</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={item.openModal()} style={[styles.exerciseButton, styles.practiceButton]}>
                        <Text style={styles.actionText}>Executar</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }

    goToNewExercise = () => {
        this.props.navigation.navigate("Novo Treino");
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList style={{backgroundColor: "#e4e4e4"}}
                data={this.state.exercises}
                renderItem={this.renderListItem}
                keyExtractor={item => item.id}
                />
                <TouchableOpacity onPress={this.goToNewExercise} style={styles.button}>
                    <Text style={styles.buttonText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    actionText: {
        fontWeight: "500",
        fontSize: 16,
        textAlign: "center",
        color: "#fff"
    },
    exerciseButton: {
        borderStyle: 'solid',
        borderRadius: 5,
        borderWidth: 1,
        padding: 10,
        width: 100
    },
    removeButton: {
        borderColor: "#000",
        backgroundColor: "#580000"
    },
    practiceButton: {
        borderColor: "#000",
        backgroundColor: "#72bcd4"
    },
    actionButtons: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        width: "100%",
        paddingVertical: 5
    },
    container: {
        backgroundColor: '#fff',
        height: '100%',
        width: '100%'
    },
    button: {
        borderRadius: 400,
        borderColor: "#000",
        borderWidth: 2,
        width: 40,
        height: 40,
        backgroundColor: "#72bcd4",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 65,
        right: 35
    },
    buttonText: {
        fontSize: 20,
        fontWeight: "bold"
    },
    listStyle: {
        width: '100%',
        height: "auto",
        backgroundColor: "#fff",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-start",
        borderBottomColor: "#000",
        borderBottomWidth: 2,
        borderStyle: "solid"
    },
    listText: {
        fontWeight: "bold",
        fontSize: 20,
        textTransform: "capitalize"
    }
})