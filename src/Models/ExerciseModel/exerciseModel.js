import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';

export default class ExerciseModel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exerciseId: props.id,
            childExercises: props.list,
            expanded: false
        }
    }


    toggle = () => {
        console.log('toggle --');
        this.setState({expanded: !this.state.expanded})
    }

    renderItem = ({item}) => {
        let retorno = null;
        if(this.state.expanded) {
            retorno = <Text>{item.name}</Text>
        }
        return retorno;
    }

    render() {
        console.log('render state --- ', this.state);
        let symbol = "+";
        let data = [];
        
        if(this.state.expanded) {
            symbol = "-";
            data = this.state.childExercises;
        }
        return (
            <View style={styles.listStyle}>
                <TouchableOpacity onPress={() => this.toggle()} style={styles.listItem}>
                    <Text style={styles.listText}>{this.state.exerciseId}</Text>
                    <Text style={styles.listText}> {symbol} </Text>
                </TouchableOpacity>
                {this.state.expanded ? 
                <FlatList data={this.state.childExercises}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.name}
                /> : null}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    listStyle: {
        width: '100%',
        height: "auto",
        backgroundColor: "#fff",
        paddingVertical: 15,
        paddingHorizontal: 25,
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
    },
    listText: {
        fontWeight: "bold",
        fontSize: 20,
        textTransform: "capitalize"
    },
    listItem: {
        display: "flex",
        width: '100%',
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        textAlign: "center",
    }
})