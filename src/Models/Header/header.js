import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import Drawer from 'react-native-drawer';
import React, {Component} from 'react';
import icon from '../../../assets/list.png'

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: props.text
        }
    }

handleIconClick = () => {
    console.log('testeeeee', this);
}


render() {
    return (
            <View style={styles.headerBar}>
                <Text style={styles.headerText}>{this.state.text}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    headerBar: {
        width: '100%',
        height: 60,
        display: "flex",
        backgroundColor: "#72bcd4",
        alignItems: "center",
        justifyContent: "space-between",
        paddingVertical: 15,
        borderBottomColor: "#000",
        borderBottomWidth: 2,
        borderStyle: "solid"
    },
    headerText: {
        fontSize: 24,
        color: '#000',
        fontWeight: "bold"
    },
    imageButton: {
        backgroundColor: 'red'
    }
})